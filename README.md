[![pipeline status](https://gitlab.com/autotesting2/cucumber-starter/badges/main/pipeline.svg)](https://gitlab.com/autotesting2/cucumber-starter/-/commits/main)
# Getting started with Serenity and Cucumber

Serenity BDD is a library that makes it easier to write high quality automated acceptance tests, with powerful reporting and living documentation features. It has strong support for both web testing with Selenium, and API testing using RestAssured.

Serenity strongly encourages good test automation design, and supports several design patterns, including classic Page Objects, the newer Lean Page Objects/ Action Classes approach, and the more sophisticated and flexible Screenplay pattern.

The latest version of Serenity supports Cucumber 7.1.

## Getting started
For creating new scenarios, clone this repo on your workstation by command `git clone https://gitlab.com/autotesting2/cucumber-starter.git` or download as [zip](https://gitlab.com/autotesting2/cucumber-starter/-/archive/main/cucumber-starter-main.zip). Wait for your IDE to initialize project and download dependencies. From this point you can start. There are 2 ways to write new cases:
1. Use existing test methods for testing described endpoints. You should expand test suite by adding new parametrized steps in `src/test/resources/reatures` as a new feature file (see [The sample scenario](#the-sample-scenario))
2. Write new test methods for creating new scenarios and steps. In this case you can create new test methods in `src/test/java/starter/stepdefinitions/SearchStepDefinition.java` or create new class with definitions in `src/test/java/starter/stepdefinitions/`. (see [Writing steps](#writing-steps))


### The project directory structure
The project has build scripts for both Maven and Gradle, and follows the standard directory structure used in most Serenity projects:
```Gherkin
src
  + main
  + test
    + java                        Test runners and supporting code
    + resources
      + features                  Feature files
     + search                  Feature file subdirectories 
             search_by_keyword.feature
```

Serenity 2.2.13 introduced integration with WebdriverManager to download webdriver binaries.

## The sample scenario
This project uses the sample Cucumber scenario. In this scenario, Sergey (who likes to search for stuff) is performing a search on the internet:

```Gherkin
Feature: Search by keyword

  Scenario: Searching for a term
    Given Sergey is researching things on the internet
    When he looks up "Cucumber"
    Then he should see information about "Cucumber"
```


## Writing steps
For creating the code of test steps actions, Cucumber Java providing `Given-When-Then` pattern of codding. In this pattern we should specify our test logic as: Given - our Pre-Condition, When - actions of testing, Then - validating results. Here is an example of simple `Given-When-Then` test:

```Java
package hellocucumber;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import static org.junit.jupiter.api.Assertions.*;

class IsItFriday {
    static String isItFriday(String today) {
        return null;
    }
}

public class Stepdefs {
    private String today;
    private String actualAnswer;

    @Given("today is Sunday")
    public void today_is_Sunday() {
        today = "Sunday";
    }

    @When("I ask whether it's Friday yet")
    public void i_ask_whether_it_s_Friday_yet() {
        actualAnswer = IsItFriday.isItFriday(today);
    }

    @Then("I should be told {string}")
    public void i_should_be_told(String expectedAnswer) {
        assertEquals(expectedAnswer, actualAnswer);
    }
}
```

## Executing the tests
To run the sample project, you can either just run the `CucumberTestSuite` test runner class, or run either `mvn verify` from the command line.

```shell
mvn clean verify
```

The test results will be recorded in the `target/site/serenity` directory.

## Generating the reports
Since the Serenity reports contain aggregate information about all of the tests, they are not generated after each individual test (as this would be extremenly inefficient). Rather, The Full Serenity reports are generated by the `serenity-maven-plugin`. You can trigger this by running `mvn serenity:aggregate` from the command line or from your IDE.

They reports are also integrated into the Maven build process: the following code in the `pom.xml` file causes the reports to be generated automatically once all the tests have completed when you run `mvn verify`?

```xml
             <plugin>
                <groupId>net.serenity-bdd.maven.plugins</groupId>
                <artifactId>serenity-maven-plugin</artifactId>
                <version>${serenity.maven.version}</version>
                <configuration>
                    <tags>${tags}</tags>
                </configuration>
                <executions>
                    <execution>
                        <id>serenity-reports</id>
                        <phase>post-integration-test</phase>
                        <goals>
                            <goal>aggregate</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
```
## Changes
1. Removed unnecessary configuration files based on the task
2. Updated versions of dependencies
3. Fixed bugs in test scenario, when checking rule was incorrect
4. Divided test scenario on positive and negative
5. Added scenarios names for fixing default Serenity HTML Report
6. Commented unnecessary webdriver cofigs
7. Added GitLab-CI Pipeline
8. Removed unnecessary documentation from README
