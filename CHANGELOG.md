1.0.0:
    1. Removed unnecessary configuration files based on the task
    2. Updated versions of dependencies
    3. Fixed bugs in test scenario, when checking rule was incorrect
    4. Divided test scenario on positive and negative
    5. Added scenarios names for fixing default Serenity HTML Report
    6. Commented unnecessary webdriver cofigs
    7. Added GitLab-CI Pipeline
    8. Removed unnecessary documentation from README